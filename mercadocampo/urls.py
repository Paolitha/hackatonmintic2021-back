"""mercadocampo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from mercadocampoApp.views import UltimasCalificacionesView, CalificacionListCreate
from mercadocampoApp.views import PersonaListCreate, PersonaRetrieveUpdateDelete
from mercadocampoApp.views import OfertaListCreate, OfertaRetrieveUpdateDelete
from mercadocampoApp.views import VentaRetrieveUpdateDelete, VentaListCreate

urlpatterns = [
    path('admin/', admin.site.urls),
    path('registro/', PersonaListCreate.as_view()),
    path('perfil/<int:pk>', PersonaRetrieveUpdateDelete.as_view()),
    path('home/', OfertaListCreate.as_view()),
    path('comprar/<int:pk>', OfertaRetrieveUpdateDelete.as_view()),
    path('transaccion/<int:pk>', VentaRetrieveUpdateDelete.as_view()),
    path('agregaroferta/', OfertaListCreate.as_view()),
    path('calificar/', CalificacionListCreate.as_view()),
    path('comprar/', VentaListCreate.as_view()),
    # Para probar la vista de calificaciones
    path('prueba/<int:perfilid>', UltimasCalificacionesView.as_view())
]
