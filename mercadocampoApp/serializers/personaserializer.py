from rest_framework import serializers
from mercadocampoApp.models.persona import Persona

class PersonaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Persona
        fields = [
            "pers_pk",
            "pers_id",
            "pers_doc",
            "pers_perfil",
            "pers_nombre",
            "pers_apellido",
            "pers_email",
            "pers_direccion",
            "pers_telefono1",
            "pers_telefono2",
            "pers_ciudad",
            "pers_departamento"
        ]