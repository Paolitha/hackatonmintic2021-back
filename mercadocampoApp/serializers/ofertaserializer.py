from rest_framework import serializers
from mercadocampoApp.models.oferta import Oferta

class OfertaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Oferta
        fields = [
            "of_pk",
            "of_campesinopk",
            "of_producto",
            "of_valorporunidad",
            "of_presentacion",
            "of_cantidad"
        ]