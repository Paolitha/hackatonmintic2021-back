from rest_framework import serializers
from mercadocampoApp.models.userlogin import Login

class LoginSerializer(serializers.ModelSerializer):

    class Meta:
        model = Login
        fields = [
            "log_pk",
            "log_personapk",
            "log_password"
        ]