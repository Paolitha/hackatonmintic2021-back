from rest_framework import serializers
from mercadocampoApp.models.calificacion import Calificacion

class CalificacionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Calificacion
        fields = [
            "calidcalificacion",
            "calidcalificado",
            "calidcalificador",
            "calperfilcalificado",
            "calcalificacion",
            "calfechacalificacion",
            "calidventa"
        ]