from rest_framework import serializers
from mercadocampoApp.models.venta import Venta

class VentaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Venta
        fields = [
            "venidventa",
            "venidproducto",
            "vencantidadproducto",
            "venidcomprador",
            "venfechaventa",
            "venestadoventa",
            "venvalortotal"
        ]
