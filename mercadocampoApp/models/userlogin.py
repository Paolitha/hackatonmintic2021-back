from django.db import models

from mercadocampoApp.models.persona import Persona

class Login(models.Model):
    log_pk = models.AutoField(primary_key = True)
    log_personapk = models.ForeignKey(
        Persona, 
        related_name = "log_pers_pk",
        on_delete = models.CASCADE,
        null = False
    )
    log_password = models.TextField(max_length = 100)