from django.db import models
from .oferta import Oferta
from .persona import Persona

class Venta(models.Model):
    venidventa = models.AutoField(primary_key=True)
    venidproducto=models.ForeignKey(Oferta, related_name='ven_of_pk', on_delete=models.CASCADE)
    vencantidadproducto=models.FloatField(default=0.0)
    venidcomprador=models.ForeignKey(Persona, related_name="ven_pers_pk", on_delete=models.CASCADE)
    venfechaventa=models.DateTimeField()
    venestadoventa=models.TextField(choices= (("A", "Aprobada"),("P", "Pendiende de aprobacion"),("F","Finalizada"),("C", "Cancelada")))
    venvalortotal=models.IntegerField(default=0)
