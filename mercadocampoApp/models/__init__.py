from .user import User
from .calificacion  import Calificacion
from .persona import Persona
from .oferta import Oferta
from .userlogin import Login
from .venta import Venta

