from django.db import models

from mercadocampoApp.models.persona import Persona

class Oferta(models.Model):
    of_pk = models.AutoField(primary_key = True)
    of_campesinopk = models.ForeignKey(
        Persona,
        related_name = "of_pers_pk",
        on_delete=models.CASCADE,
        null=False
    )
    of_producto = models.TextField(max_length = 50)
    of_valorporunidad = models.IntegerField()
    of_presentacion = models.TextField(choices = (
        ("U", "Unidad"),
        ("P", "Peso")
    ))
    of_cantidad = models.FloatField()