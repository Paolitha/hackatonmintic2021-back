from django.db import models

class Persona(models.Model):
    pers_pk = models.AutoField(primary_key = True)
    pers_id = models.TextField(max_length = 30)
    pers_doc = models.TextField(choices = (
        ("C", "Cédula de ciudadanía"),
        ("E", "Cédula de extranjería"),
        ("P", "Pasaporte")
    ))
    pers_perfil = models.TextField(choices = (
        ("O", "Comprador"),
        ("A", "Campesino")
    ))
    pers_nombre = models.TextField(max_length = 50)
    pers_apellido = models.TextField(max_length = 100)
    pers_email = models.TextField(max_length = 50)
    pers_direccion = models.TextField(max_length = 100)
    pers_telefono1 = models.TextField(max_length = 20)
    pers_telefono2 = models.TextField(max_length = 20, null = True)
    pers_ciudad = models.TextField(max_length = 30)
    pers_departamento = models.TextField(max_length = 30)