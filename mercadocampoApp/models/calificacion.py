from django.db import models

from .persona import Persona
from .user import User
from .venta import Venta

class Calificacion(models.Model):
    calidcalificacion = models.AutoField(primary_key=True)
    calidcalificado = models.ForeignKey(Persona, related_name="cal_pers_pk_calificado", on_delete=models.CASCADE)
    calidcalificador = models.ForeignKey(Persona, related_name="cal_pers_pk_calificador", on_delete=models.CASCADE)
    calperfilcalificado = models.CharField(choices=(("A","Campesino"), ("O", "Comprador")),max_length=50)
    calcalificacion = models.IntegerField('Calificacion')
    calfechacalificacion =  models.DateField('Fecha de Calificacion')
    calidventa = models.ForeignKey(Venta, related_name='cal_ven_id_venta', on_delete=models.CASCADE)