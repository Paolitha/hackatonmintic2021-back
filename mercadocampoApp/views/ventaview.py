from django.db.models.query import QuerySet
from rest_framework import serializers, views, status
from rest_framework.response import Response
from mercadocampoApp.models.venta import Venta
from mercadocampoApp.serializers.ventaSerializer import VentaSerializer
from rest_framework import generics

# GET para ver los datos de la transaccion
class VentaRetrieveUpdateDelete(generics.RetrieveUpdateDestroyAPIView):
    queryset = Venta.objects.all()
    serializer_class = VentaSerializer

# POST para realizar cona compra/venta
class VentaListCreate(generics.ListCreateAPIView):
    queryset = Venta.objects.all()
    serializer_class = VentaSerializer