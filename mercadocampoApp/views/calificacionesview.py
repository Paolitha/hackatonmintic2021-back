from django.db.models.query import QuerySet
from rest_framework import serializers, views, status
from rest_framework.response import Response
from mercadocampoApp.models.calificacion import Calificacion
from mercadocampoApp.serializers import CalificacionSerializer
from rest_framework import generics

# Para mostrar las calificaciones de un perfil en especifico
class UltimasCalificacionesView(views.APIView):
    
    def get(self, request):

        id_perfil = request.perfilid
        queryset = Calificacion.objects.get(calidcalificado = id_perfil)
        serialized = CalificacionSerializer(queryset, many = True)

        return Response(serialized.data, status = status.HTTP_200_OK)

# POST para agregar una calificacion
class CalificacionListCreate(generics.ListCreateAPIView):
    queryset = Calificacion.objects.all()
    serializer_class = CalificacionSerializer

        