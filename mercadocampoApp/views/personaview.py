from django.db.models.query import QuerySet
from rest_framework import serializers, views, status
from rest_framework.response import Response
from mercadocampoApp.models.persona import Persona
from mercadocampoApp.serializers.personaserializer import PersonaSerializer
from rest_framework import generics

# POST se usaria en Registro
class PersonaListCreate(generics.ListCreateAPIView):
    queryset = Persona.objects.all()
    serializer_class = PersonaSerializer

# GET se usaria en perfil campesino y perfil comprador
class PersonaRetrieveUpdateDelete(generics.RetrieveUpdateDestroyAPIView):
    queryset = Persona.objects.all()
    serializer_class = PersonaSerializer
