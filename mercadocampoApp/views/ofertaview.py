from django.db.models.query import QuerySet
from rest_framework import serializers, views, status
from rest_framework.response import Response
from mercadocampoApp.models.oferta import Oferta
from mercadocampoApp.serializers.ofertaserializer import OfertaSerializer
from rest_framework import generics

# GET para mostrar las ofertas en home
# POST para agregar una oferta
class OfertaListCreate(generics.ListCreateAPIView):
    queryset = Oferta.objects.all()
    serializer_class = OfertaSerializer

# GET para ver los datos del producto a mostrar
class OfertaRetrieveUpdateDelete(generics.RetrieveUpdateDestroyAPIView):
    queryset = Oferta.objects.all()
    serializer_class = OfertaSerializer