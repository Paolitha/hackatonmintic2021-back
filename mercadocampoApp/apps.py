from django.apps import AppConfig


class MercadocampoappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mercadocampoApp'
