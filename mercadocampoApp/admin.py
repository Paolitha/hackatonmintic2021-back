from django.contrib import admin
from .models.venta import Venta
from .models.calificacion import Calificacion
from .models.persona import Persona
from .models.oferta import Oferta
from .models.userlogin import Login

# Register your models here.
admin.site.register(Calificacion)
admin.site.register(Persona)
admin.site.register(Oferta)
admin.site.register(Login)
admin.site.register(Venta)